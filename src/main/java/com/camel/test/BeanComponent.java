package com.camel.test;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BeanComponent {

    List<bean> beanList = new ArrayList<>();


    public  List<bean> sayHello() {
        beanList.add(new bean("a","b","c"));
        beanList.add(new bean("d","e","f"));
        return beanList;
    }


    class bean {
        public bean(String a, String b, String c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public String a;
        public String b;
        public String c;

        @Override
        public String toString() {
            return "bean{" +
                    "a='" + a + '\'' +
                    ", b='" + b + '\'' +
                    ", c='" + c + '\'' +
                    '}';
        }
    }


}
