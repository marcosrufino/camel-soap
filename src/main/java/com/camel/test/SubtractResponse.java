package com.camel.test;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "SubtractResponse")
@XmlAccessorType (XmlAccessType.FIELD)
public class SubtractResponse
{
    private int SubtractResult;
}