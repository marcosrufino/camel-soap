package com.camel.test;

import lombok.Data;

/**
 * @author Higor Tavares
 */
@Data
public class Add {
    private int A;
    private int B;
}
