package com.camel.test;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteBean extends RouteBuilder {
    @Override
    public void configure() throws Exception {

        from("timer:app?period=10s")
                .bean(BeanComponent.class,"sayHello")
                .split(body())
                .convertBodyTo(String.class).log("${body}");

    }
}
