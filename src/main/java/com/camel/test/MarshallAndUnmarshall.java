package com.camel.test;

import com.thoughtworks.xstream.XStream;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.xstream.XStreamDataFormat;
import org.springframework.stereotype.Component;

//@Component
public class MarshallAndUnmarshall extends RouteBuilder {


    @Override
    public void configure() throws Exception {
        final XStream xstream = new XStream();
        xstream.alias("AddResponse", AddResponse.class);
        Add add = new Add();
        add.setA(4);
        add.setB(8);
        from("timer:application?period=10s")
                .process(exchange -> {
                    exchange.getIn().setBody(add);

                })
                .log("${body}")
                .marshal()
                .jacksonxml()
                .to("xslt:Add.xslt")
                .log("${body}")
                .convertBodyTo(String.class)
                .to("spring-ws:http://www.dneonline.com/calculator.asmx?webServiceTemplate=#webServiceTemplate&soapAction=http://tempuri.org/Add")
                .log("${body}")
                .convertBodyTo(String.class)
                .unmarshal(new XStreamDataFormat(xstream))
                .log("${body}");
    }
}
