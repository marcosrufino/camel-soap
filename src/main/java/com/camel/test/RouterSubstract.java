package com.camel.test;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;


//@Component
public class RouterSubstract extends RouteBuilder {


    @Override
    public void configure() throws Exception {
        JaxbDataFormat xmlDataFormat = new JaxbDataFormat();
        JAXBContext con = JAXBContext.newInstance(SubtractResponse.class);
        xmlDataFormat.setContext(con);

   /*     from("timer:application?period=10s")
                .setBody(simple("<Subtract xmlns=\"http://tempuri.org/\">\n" +
                        "      <intA>10</intA>\n" +
                        "      <intB>2</intB>\n" +
                        "    </Subtract>"))
                .to("spring-ws:http://www.dneonline.com/calculator.asmx?webServiceTemplate=#webServiceTemplate&soapAction=http://tempuri.org/Subtract")
                .log("${body}")
                .to("xslt:soap-tag-remove.xslt.xsl")
                .unmarshal(xmlDataFormat)
                .process(exchange -> {
                    SubtractResponse subtractResponse = exchange.getIn().getBody(SubtractResponse.class);
                    System.out.println(subtractResponse.getSubtractResult());
                })
                //.transform().xpath("/n:SubtractResponse/text()", new Namespaces("n", "http://tempuri.org/"))
                .log("The title is: ${body}")
                //.convertBodyTo(String.class)
                //.transform().xpath("/SubtractResponse/")
                .log("${body}");
        //.setBody(constnt("12345"))
*/


        from("timer:application?period=20s")
                .process(exchange -> {
                    Subtract subtract = new Subtract(10, 5);
                    exchange.getIn().setBody(subtract);
                })
                .marshal(xmlDataFormat)
                .log("${body}")

                //.transform().xpath("/n:SubtractResponse/text()", new Namespaces("n", "http://tempuri.org/"))
                .log("The title is: ${body}")
                //.convertBodyTo(String.class)
                //.transform().xpath("/SubtractResponse/")
                .log("${body}");
        //.setBody(constnt("12345"))


    }
}
