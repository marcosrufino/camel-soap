package com.camel.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class SoapConfig {

    @Bean(value = "webServiceTemplate")
    public WebServiceTemplate creatWebServiceTemplate(){
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setDefaultUri("http://www.dneonline.com/calculator.asmx");
        return webServiceTemplate;
    }

}
