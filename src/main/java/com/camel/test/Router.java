package com.camel.test;

import com.thoughtworks.xstream.XStream;
import org.apache.camel.builder.RouteBuilder;

import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.converter.jaxb.JaxbDataFormat;

import org.apache.camel.dataformat.soap.name.ElementNameStrategy;
import org.apache.camel.dataformat.soap.name.ServiceInterfaceStrategy;
import org.apache.camel.dataformat.soap.name.TypeNameStrategy;
import org.apache.camel.model.dataformat.SoapJaxbDataFormat;
import org.apache.camel.model.dataformat.XStreamDataFormat;
import org.apache.camel.support.builder.Namespaces;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import java.util.HashMap;
import java.util.Map;

//@Component
public class Router extends RouteBuilder {


    @Override
    public void configure() throws Exception {

        JaxbDataFormat xmlDataFormat = new JaxbDataFormat();
        JAXBContext con = JAXBContext.newInstance(SubtractResponse.class);
        xmlDataFormat.setContext(con);

        from("timer:application?period=10s")
                .setBody(simple("<Subtract xmlns=\"http://tempuri.org/\">\n" +
                        "      <intA>10</intA>\n" +
                        "      <intB>2</intB>\n" +
                        "    </Subtract>"))
                .to("spring-ws:http://www.dneonline.com/calculator.asmx?webServiceTemplate=#webServiceTemplate&soapAction=http://tempuri.org/Subtract")
                .log("${body}")
                .to("xslt:soap-tag-remove.xslt.xsl")
                .unmarshal(xmlDataFormat)
                .process(exchange -> {
                    SubtractResponse subtractResponse = exchange.getIn().getBody(SubtractResponse.class);
                    System.out.println(subtractResponse.getSubtractResult());
                })
                //.transform().xpath("/n:SubtractResponse/text()", new Namespaces("n", "http://tempuri.org/"))
                .log("The title is: ${body}")
                //.convertBodyTo(String.class)
                //.transform().xpath("/SubtractResponse/")
                .log("${body}");
                //.setBody(constnt("12345"))


    }
}
