package com.camel.test;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Subtract {

    private int intA;
    private int intB;

}
