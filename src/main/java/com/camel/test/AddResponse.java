package com.camel.test;

import lombok.Data;

/**
 * @author Higor Tavares
 */
@Data
public class AddResponse {
    private int AddResult;
    @Override
    public String toString(){
        return "A soma é: "+this.getAddResult();
    }
}
