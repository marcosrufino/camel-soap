<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="no" indent="yes"/>
    <xsl:template match="/Add">
        <Add xmlns="http://tempuri.org/">
            <intA><xsl:value-of select="a"/></intA>
            <intB><xsl:value-of select="b"/></intB>
        </Add>
    </xsl:template>
</xsl:stylesheet>
